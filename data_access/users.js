 (function() {
     function getUsersList(select, options, helpers, cb) {
         helpers.getDbClient(function(error, dbClient) {
             if (!error) {
                 let user = dbClient.collection('users');

                 user.find(select).toArray(function(retrievalErr, users) {
                     if (!retrievalErr) {
                         helpers.execute(cb, [null,users]);
                     }
                     else{
                        helpers.execute(cb, [retrievalErr,null]);
                    }
                 });

             } else {
                 cb({
                     "error": "Error while connecting to mongoDB"
                 })
             }


         })

     }
     exports.Users = {
         getUsersList: getUsersList
     }

 })()
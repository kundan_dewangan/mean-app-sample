var app = require('express')()
var bodyparser = require('body-parser');
var morgan = require('morgan');
var validate_user_post = require('./api/validate_user_post')
var helpers = require('./helpers').helpers;


//body parser
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));


app.use(morgan('dev'));

app.use('/api',validate_user_post);
require('./router').establishRoutes(app, helpers);

app.listen(5000)
console.log('port number 5000 is running')